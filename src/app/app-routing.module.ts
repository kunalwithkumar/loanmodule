import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { EducationLoanComponent } from './education-loan/education-loan.component';
import { EmiCalculatorComponent } from './emi-calculator/emi-calculator.component';
import { ErrorComponent } from './error/error.component';
import { FAQsComponent } from './faqs/faqs.component';
import { HomeLoanComponent } from './home-loan/home-loan.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { OtherLoansComponent } from './other-loans/other-loans.component';
import { PersonalLoanComponent } from './personal-loan/personal-loan.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { VehicleLoanComponent } from './vehicle-loan/vehicle-loan.component';

const routes: Routes = [
  { path: 'user', component: UserComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'faqs', component: FAQsComponent },
  { path: 'emi-cal', component: EmiCalculatorComponent },
  { path: 'edu-loan', component: EducationLoanComponent },
  { path: 'home-loan', component: HomeLoanComponent },
  { path: 'other-loan', component: OtherLoansComponent },
  { path: 'personal-loan', component: PersonalLoanComponent },
  { path: 'v-loan', component: VehicleLoanComponent },
  { path: 'error', component:  ErrorComponent}




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
