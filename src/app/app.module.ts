import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin/admin.component';
import { EmiCalculatorComponent } from './emi-calculator/emi-calculator.component';
import { PersonalLoanComponent } from './personal-loan/personal-loan.component';
import { HomeLoanComponent } from './home-loan/home-loan.component';
import { OtherLoansComponent } from './other-loans/other-loans.component';
import { VehicleLoanComponent } from './vehicle-loan/vehicle-loan.component';
import { EducationLoanComponent } from './education-loan/education-loan.component';
import { FAQsComponent } from './faqs/faqs.component';
import { ErrorComponent } from './error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    AdminComponent,
    EmiCalculatorComponent,
    PersonalLoanComponent,
    HomeLoanComponent,
    OtherLoansComponent,
    VehicleLoanComponent,
    EducationLoanComponent,
    FAQsComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
